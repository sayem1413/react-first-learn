import React, { Component } from 'react';

class Counter extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            count: 20,
            color: 'green',
        }
    }

    decrease = () => {
        this.setState({
            count: this.state.count - 1
        })
        if(this.state.count < 18){
            this.setState({
                color: 'red'
            })
        }
    }

    increase = () => {
        this.setState({
            count: this.state.count + 1
        })
        if(this.state.count > 22){
            this.setState({
                color: 'red'
            })
        }
    }

    render(){
        
        return (
            <div style={{color:this.state.color}}>
                <span>
                    <button onClick={ this.decrease }>
                        -
                    </button>
                </span>
                <p>{ this.state.count }</p>
                <span>
                    <button onClick={ this.increase }>
                        +
                    </button>
                </span>
            </div>
            
            );
    }

}

export default Counter;