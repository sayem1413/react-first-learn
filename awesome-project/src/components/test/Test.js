import React from 'react';
import { Link  } from 'react-router-dom';
function Test({match}) {
    return (
      <div>
        <h2>Test {match.params.postId}</h2>
        <Link to='/'>Back to Home</Link>
      </div>
    );
  }
export default Test;