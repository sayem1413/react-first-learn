import React, { Component } from 'react';
import './App.css';
// import logo from '../logo.svg';
// import First from './first/First';
// import Counter from './counter/Counter';
// import StateLessComponent from './stateLessComponent/StateLessComponent';
// import Books from './books/Books';
// import axios from 'axios';
// import PostForm from './forms/PostForm';
// import PostApiForm from './post/PostApiForm';

// import { BrowserRouter as Router, Route, Switch  } from 'react-router-dom';

// import Nav from './nav/Nav';
// import Home from './home/Home';
// import About from './about/About';
// import Contact from './contact/Contact';
// import Test from './test/Test';
// import PrivateRoute from '../PrivateRoute'
// import Login from './login/Login';
// import Logout from './login/Logout';

import Count from './count-redux/Count';
import Control from './count-redux/Control';
import { Provider } from 'react-redux';
import store from './store/index';

//======== React and Redux 

class App extends Component{

  render(){



    return (
      <Provider store={ store } >
        <div className="App">
          <Count/>
          <Control/>
        </div>
      </Provider>
    );
  }
}

// React will be responsible for only view layer
// Redux will be responsible for only Data layer
// React-Redux library

//======== Redux 

// class App extends Component{

//   render(){

//     // A reducer must have two parameter
//     // State , Action
//     const reducer = (state={} , action) => {
//       if(action.type === 'A'){
//         return{
//           ...state,
//           A: 'I am A'
//         }         
//       }
//       if(action.type === 'B'){
//         return{
//           ...state,
//           B: 'I am B'
//         }
//       }
//       return state;
//     }

//     const store = createStore(reducer)

//     store.subscribe( () => {
//       console.log(store.getState());
//     })

//     // store.subscribe( () => {
//     //   console.log(store.getState().A);
//     // })

//     // store.subscribe( () => {
//     //   console.log(store.getState().B);
//     // })

//     store.dispatch({ type: 'Something' });
//     store.dispatch({ type: 'A' });
//     store.dispatch({ type: 'Something' });
//     store.dispatch({ type: 'B' });
//     store.dispatch({ type: 'Something' });

//     return <div className="App">
//       <h1>Hello Programmers!</h1>
//     </div>
//   }
// }

// store -> will hold all of our application data/state
// reducer -> A function who returns a specific amount of state or data
// Actions -> Event occurs -> { type: 'Something' Payload: 'anything' }
// Dispatch
// Subscriber


// 1. Create a reducer
// 2. Create store with the help of reducer
// 3. Now we can subscribe
// 4. Dispatch(action)


//======== React route 

// class App extends Component{

//   render(){
//     return (
//       <Router>
//         <div className="App">
//           <Nav/>
//           <Switch>
//             {/* <Route exact path="/" component={ Home } /> */}
//             <Route exact path="/" render={() => {
//               return <Home name='For practice'></Home>
//             }} />
//             <Route path="/about" component={ About } />
//             <Route path="/contact" component={ Contact } />
//             <PrivateRoute path="/posts/:postId" component={ Test } />
//             <Route path="/login" component={ Login } />
//             <Route path="/logout" component={ Logout } />

//           </Switch>
//         </div>
        
//       </Router>
//     );
//   }
// }

//========= React Forms Post

// class App extends Component{

//   state = {
//     posts: [

//     ]
//   }

//   render(){
//       return (
//         <div className='container'>
//               <div className='row'>
//                 <div className='col-md-8 offset-sm-2'>
//                   <PostApiForm/>
//                 </div>
//               </div>
//         </div>
//       );
//   }
// }


//========= React Forms

// class App extends Component{

//     state = {
//       posts: [

//       ]
//     }

//     render(){
//         // // console.log(this.state);
//         // let { posts } = this.state;
//         // if( this.state.posts.length === 0){
//         //   return <h1 style={{ textAlign: 'center' }}>Loading......</h1>
//         // } else {
//             return <div className='container'>
//                   <div className='row'>
//                     <div className='col-md-8 offset-sm-2'>
//                       <PostForm/>
//                     </div>
//                   </div>
//             </div>
//         // }
      
//     }
// }

//======== Axios in React 

// class App extends Component{

//   state = {
//     posts: [

//     ]
//   }

//   componentDidMount(){
//     axios.get('https://jsonplaceholder.typicode.com/posts')
//           .then(res => {
//             this.setState({
//               posts: res.data
//             })
//           })
//           .catch(e => console.log(e))
//   }

//   render(){
//     // console.log(this.state);
//     let { posts } = this.state;
//     if( this.state.posts.length === 0){
//       return <h1 style={{ textAlign: 'center' }}>Loading......</h1>
//     } else {
//         return <div className='container'>
//               <ul className='list-group'>
//                   { posts.map( post => <li key={ post.id } className='list-group-item'>{ post.title }</li> ) }
//               </ul>
//         </div>
//     }


//   }
// }

//=========== Life Cycle

// 1. Constructor
// 2. Render
// 3. Call all child Lifecycle method
// 4. ComponentDidMount
// 5. ComponentDidUpdate
// 6. ComponentWillUnmount

// class App extends Component{
//   constructor(){
//     super()
//     console.log('1. App Contructor');
//   }
//   componentDidMount(){
//     console.log('3. App ComponentDidMount')
//   }
//   componentDidUpdate(){
//     console.log('4. App ComponentDidUpdate')
//   }
//   componentWillUnmount(){
//     console.log('Unmount')
//   }
//   render(){
//     console.log('2. App Rendered')
//     return <div className="App">
//       <h1>Hello Programmers!</h1>
//     </div>
//   }
// }

//============= list and events in Books Component

// class App extends Component{
//   state = {
//     books: [
//       {
//         id: 1,
//         name: 'Javascript',
//         price: 2016
//       },
//       {
//         id: 2,
//         name: 'vue.js',
//         price: 2018
//       },
//       {
//         id: 3,
//         name: 'react.js',
//         price: 2019
//       },
//     ]
//   }

//   deleteHandler = (id) => {
//     let newBooks = this.state.books.filter( book => book.id != id);
//     this.setState({
//       books: newBooks
//     })
//   }

//   updateHandler = (name, id) => {
//     let newBooks = this.state.books.map( book => {
//       if( id === book.id ){
//         return {
//           ...book,
//           name
//         }
//       }

//       return book;
//     });
//     this.setState({
//       books: newBooks
//     })
//   }

//   render(){
//     return <div className="App">
//       <Books
//           updateHandler={ this.updateHandler.bind(this) }
//           deleteHandler={ this.deleteHandler.bind(this) }
//           books={this.state.books} />
//     </div>
//   }
// }

//======== Style 

// class App extends Component{

//   render(){
//     return <div className="App">
//       <h1>Hello Programmers!</h1>
//     </div>
//   }
// }

//========== Stateless Component

// class App extends Component{

//   render(){
//     return <div className="App">
//       <StateLessComponent name="Abu Shahadat Md. Sayem"/>
//     </div>
//   }
// }

//========== Event Handler

// class App extends Component{

//   state = {
//     name: '',
//   }

//   // eventHandler = (event) => {
//   //   console.log(event.target);
//   // }

//   inputHandler = (event) => {
//     this.setState({
//       name: event.target.value
//     })
//   }

//   render(){
//     return <div className="App">
//       <div className="container mt-4">
//         <input onChange={ this.inputHandler } className="form-control" type="text" placeholder="Enter your name"/>
//         <button className="btn btn-primary" onClick={(event) => { console.log(event.target) }}>
//           Click Me
//         </button>
//         {this.state.name ? <p>Hello, { this.state.name }</p> : ''}
//       </div>
//     </div>
//   }
// }

// App Class Counter

// class App extends Component{

//   render(){
//     return <div className="App"><Counter/></div>
//   }
// }

//====== State

// class App extends Component{
//   state = {
//     person: [
//       { name: 'Abu Shahadat Md. Sayem1', email:'sayem1413@gmail.com1', address:'Narayanganj1'},
//       { name: 'Abu Shahadat Md. Sayem2', email:'sayem1413@gmail.com2', address:'Narayanganj2'},
//       { name: 'Abu Shahadat Md. Sayem3', email:'sayem1413@gmail.com3', address:'Narayanganj3'},
//     ]
//   };

//   render(){
//     return <div className="App">{ this.state.person.map((people, index) => {
//       return <First
//                   key={ index }
//                   name={ people.name }
//                   email={ people.email }
//                   address={ people.address }/>;
//    })}</div>
//   }
// }


//============ props

// function App() {

//   return (
//     <div className="App">
//         <First name='Abu Shahadat Md. Sayem' email='sayem1413@gmail.com' address='Narayanganj'/>
//         <First name='Abu Shahadat Md. Sayem' email='sayem1413@gmail.com' address='Narayanganj'/>
//     </div>
//   );

  // let name = 'Abu Shahadat Md. Sayem';
  // let position = 'React-learner';
  // return (
  //   <div className="App">
  //     <header className="App-header">
  //       <img src={logo} className="App-logo" alt="logo" />
  //       <div>
  //         <code>{ name }</code>
  //         <h4>{ position ? position : 'never get' }</h4>
  //       </div>
  //       <a
  //         className="App-link"
  //         href="https://reactjs.org"
  //         target="_blank"
  //         rel="noopener noreferrer"
  //       >
  //         Learn React
  //       </a>
  //     </header>
  //   </div>
  // );
  
// }

export default App;
