import React from 'react';
import { Link  } from 'react-router-dom';

function Home(props) {
    return (
      <div>
        <h2>Home {props.name}</h2>
        <Link to='/posts/1' >Post Id 1</Link>
        <Link to='/posts/2'>Post Id 2</Link>
        <Link to='/posts/3'>Post Id 3</Link>
      </div>
    );
  }
export default Home;