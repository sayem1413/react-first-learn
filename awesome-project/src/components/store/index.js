import { createStore } from 'redux';

import rootReducer from './reducers/rootReducer';

const store = createStore(rootReducer);

export default store;

// Store
//      Action
//      Reducer
// React Component
//      Child Component
//          Child Component
// 
// React-Redux Connect
//      Provoder Component
//          Store
//          App Component
//
// If we need Data
//      mapStateToProps(state) return object
//
// If we want to dispatch some action
//      mapDispatchToProps(dispatch) return object
//
// connect(mapStateToProps, mapDispatchToProps)(component)