import React, { Component } from 'react';
// import logo from '../../logo.svg';

class First extends Component {

    render(){
        console.log(this.props);

        return (
            <div>
                <h2>{ this.props.name }</h2>
                <h4>{ this.props.email }</h4>
                <h6>{ this.props.address }</h6>
            </div>
            );
        // let name = 'Abu Shahadat Md. Sayem';
        // let position = 'React-learner';
        // return <h1>Hi, I'm new at react!</h1>;
        // return React.createElement('div', {className: 'App'}, [
        //   React.createElement('header', {className: 'App-header'}, [
        //     React.createElement('img', {src: logo, className: 'App-logo', alt: 'logo'}),
        //     React.createElement('div', null,
        //       React.createElement('h2', null, this.props.name),
        //       React.createElement('h4', null, this.props.email),
        //       React.createElement('h6', null, this.props.address),
        //       React.createElement('a', {
        //         href: 'https://reactjs.org',
        //         className: 'App-link',
        //         target: '_blank',
        //         rel:'noopener noreferrer'
        //       }, 'Learn React'),
        //     )
        //   ])
        // ]);
    }

}

export default First;