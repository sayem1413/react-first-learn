import React, { Component } from 'react';

const initialState = {
    name: '',
    email: '',
    password:'',
    bio:'',
    country:'',
    gender:'',
    skills:[],
}

class PostForm extends Component{

    constructor(){
        super()

        this.myForm = React.createRef()
    }

    state = {
        ...initialState
    };

    changeHandler = event => {
        if(event.target.type === 'checkbox'){
            if( event.target.checked ){
                this.setState({
                    ...this.state,
                    skills: this.state.skills.concat(event.target.value)
                })
            } else {
                this.setState({
                    ...this.state,
                    skills: this.state.skills.filter(skill => skill !== event.target.value)
                })
            }
            
        }
        else{
            this.setState({
                [event.target.name]: event.target.value
            })
        }
    }

    submitHandler = event => {
        event.preventDefault();
        console.log(this.state)

        this.myForm.current.reset();
        this.setState({
            ...initialState
        })
    }

    render(){
        return (
            <form ref={ this.myForm } onSubmit={ this.submitHandler }>
                <div className='form-group'>
                    <label htmlFor="name">Enter your name: </label>
                    <input
                        className='form-control'
                        type='text' 
                        placeholder='Enter your name'
                        name='name'
                        id='name'
                        value={ this.state.name}
                        onChange={ this.changeHandler }
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor="email">Enter your email: </label>
                    <input
                        className='form-control'
                        type='email' 
                        placeholder='Enter your email'
                        name='email'
                        id='email'
                        value={ this.state.email}
                        onChange={ this.changeHandler }
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor="password">Enter your Password: </label>
                    <input
                        className='form-control'
                        type='password'
                        name='password'
                        id='password'
                        value={ this.state.password}
                        onChange={ this.changeHandler }
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor="bio">Enter your Short Bio: </label>
                    <textarea
                        className='form-control'
                        name='bio'
                        id='bio'
                        value={ this.state.bio }
                        onChange={ this.changeHandler }
                    > </textarea>
                </div>
                <div className='form-group'>
                    <label htmlFor="country">Select your country: </label>
                    <select
                        className='form-control'
                        name='country'
                        id='country'
                        value={ this.state.country }
                        onChange={ this.changeHandler }
                    > 
                    <option value>Select your Country</option>
                    <option value='BD'>Bangladesh</option>
                    <option value='IND'>India</option>
                    <option value='Aus'>Australia</option>
                    <option value='USA'>America</option>
                    <option value='Ger'>German</option>
                    </select>
                </div>
                <div className='form-group'>
                    <div className='form-check'>
                        <input
                            type='radio'
                            name='gender'
                            id='gender1'
                            value='male'
                            onChange={ this.changeHandler }
                        />
                        <label htmlFor="gender1">  Male </label>
                    </div>
                    <div className='form-check'>
                        <input
                            type='radio'
                            name='gender'
                            id='gender2'
                            value='female'
                            onChange={ this.changeHandler }
                        />
                        <label htmlFor="gender2">  female </label>
                    </div>
                </div>
                <div className='form-group'>
                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type='checkbox'
                            name='skills'
                            id='js'
                            value='javascript'
                            onChange={ this.changeHandler }
                        />
                        <label htmlFor="js">  Javascript </label>
                    </div>

                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type='checkbox'
                            name='skills'
                            id='vue'
                            value='vue'
                            onChange={ this.changeHandler }
                        />
                        <label htmlFor="js">  VueJS </label>
                    </div>

                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type='checkbox'
                            name='skills'
                            id='react'
                            value='react'
                            onChange={ this.changeHandler }
                        />
                        <label htmlFor="js">  ReactJS </label>
                    </div>

                    <div className='form-check'>
                        <input
                            className='form-check-input'
                            type='checkbox'
                            name='skills'
                            id='angular'
                            value='angular'
                            onChange={ this.changeHandler }
                        />
                        <label htmlFor="js">  AngularJS </label>
                    </div>
                </div>
                <button className='btn btn-primary' type='submit' > Submit </button>
                
            </form>
        );
    }
}

export default PostForm;