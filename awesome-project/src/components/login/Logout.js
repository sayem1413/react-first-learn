import React from 'react';
import { Redirect  } from 'react-router-dom';
import Auth from '../../Auth';



class Logout extends React.Component{
    state = {
        redirect: false
    }

    componentDidMount() {
        Auth.logout(() => {
            this.setState({ redirect: true })
        })
    }

    render(){
        if(this.state.redirect){
            return <Redirect to='/' />
        } else {
            return <h1> Loggin Out.... </h1>
        }
    }
}

export default Logout;