import React, { Component } from 'react';
import axios from 'axios';

const BASE_URL = 'http://jsonplaceholder.typicode.com';

const initialState = {
    title: '',
    body: '',
    userId: 111,
    isSubmitted:false,
    error: false
}

class PostApiForm extends Component{

    constructor(){
        super()

        this.myForm = React.createRef()
    }

    state = {
        ...initialState
    };

    changeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHandler = event => {
        event.preventDefault();
        axios.post(`${BASE_URL}/posts`, {
            title: this.state.title,
            body: this.state.body,
            userId: this.state.userId,
        }).then( res => {
            this.setState({
                isSubmitted: true,
                error: false,
            })
            console.log(res)
        }).catch( error => {
            this.setState({
                error: true,
                isSubmitted: false,
            })
        })
    }

    render(){
        return (
            <form ref={ this.myForm } onSubmit={ this.submitHandler }>
                <div className='form-group'>
                    <label htmlFor="title">Enter Post Title: </label>
                    <input
                        className='form-control'
                        type='text' 
                        placeholder='Enter title'
                        name='title'
                        id='title'
                        value={ this.state.title}
                        onChange={ this.changeHandler }
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor="body">Enter Post Body: </label>
                    <textarea
                        className='form-control'
                        name='body'
                        id='body'
                        value={ this.state.body }
                        onChange={ this.changeHandler }
                    > </textarea>
                </div>
                
                <button className='btn btn-primary' type='submit' > Submit </button>
                { this.state.isSubmitted && <p>Form Submited Successfully</p>  }
                { this.state.error && <p>Error Occured</p>  }
            </form>
        );
    }
}

export default PostApiForm;