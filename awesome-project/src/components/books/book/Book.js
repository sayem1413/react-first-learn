import React, { Component } from 'react';

class Book extends Component{
    state = {
        isEditAble: false
    }

    updateField = (event) => {
        if(event.key === 'Enter'){
            this.setState({ isEditAble: false })
        }
    }

    render(){
        let editOutput = this.state.isEditAble ? 
                        <input type="text"
                            onChange={ e => this.props.updateHandler(e.target.value, this.props.book.id) }
                            onKeyPress={ this.updateField }
                            value={ this.props.book.name } />
                        : <p>{ this.props.book.name }</p>
        return(
            <li className="list-group-item d-flex">
                { editOutput }
                <span className="ml-auto">{ this.props.book.price }  </span>
                <div className="mx-5">
                    <span onClick={ () => this.setState({ isEditAble: true }) } style={{ cursor: 'pointer' }} className="mx-2">
                        <i className="fas fa-edit"></i>
                    </span>
                    <span style={{ cursor: 'pointer' }} onClick={ () => this.props.deleteHandler(this.props.book.id) }>
                        <i className="fas fa-trash-alt"></i>
                    </span>
                </div>
            </li>
        )
    }
}

export default Book