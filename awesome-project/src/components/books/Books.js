import React, { Component } from 'react';
import Book from './book/Book'
class Books extends Component{
    render(){
        return(
            <div className='container mt-4'>
                { this.props.books.map( book => {
                    return(
                        <Book
                            updateHandler={ this.props.updateHandler } 
                            deleteHandler={ this.props.deleteHandler }
                            book={ book }/>
                    )
                }) }
            </div>
        )
    }
}

export default Books